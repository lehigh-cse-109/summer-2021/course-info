# CSE 109 - Summer 2021 - Bulletin Board

## :computer: Homework

- [x] 08/12 - [Homework 4](https://gitlab.com/lehigh-cse-109/summer-2021/assignments/homework-4) - pack109 ([solutions](https://gitlab.com/lehigh-cse-109/summer-2021/assignments/homework-4/-/tree/solutions))
- [x] 07/26 - [Homework 3](https://gitlab.com/lehigh-cse-109/summer-2021/assignments/homework-3) - pointers ([solutions](https://gitlab.com/lehigh-cse-109/summer-2021/assignments/homework-3/-/tree/solutions))
- [x] 07/19 - [Homework 2](https://gitlab.com/lehigh-cse-109/summer-2021/assignments/homework-2) - mywhich ([solutions](https://gitlab.com/lehigh-cse-109/summer-2021/assignments/homework-2/-/tree/solutions))
- [x] 07/09 - [Homework 1](https://gitlab.com/lehigh-cse-109/summer-2021/assignments/homework-1) - Learning Git
- [x] 07/09 - [Homework 0](https://gitlab.com/lehigh-cse-109/summer-2021/course-info/-/blob/master/Homework0.md) - Sign up for Gitlab

## :checkered_flag: Quizzes and Exams

- [ ] 08/12 - [Final Exam](https://gitlab.com/lehigh-cse-109/summer-2021/assignments)
- [x] 08/02 - [Midterm Exam](https://gitlab.com/lehigh-cse-109/summer-2021/assignments/midterm-exam)
- [x] 07/21 - [Quiz 2](https://gitlab.com/lehigh-cse-109/summer-2021/assignments/quiz-2)
- [x] 07/14 - [Quiz 1](https://gitlab.com/lehigh-cse-109/summer-2021/assignments/quiz-1)

## :books: Readings

| Readings           | 
| ------------------ |
|**Week 3**|
| <ul><li>[Learning GDB](https://www.tutorialspoint.com/gnu_debugger/index.htm)</li><li>[GDB Quick Reference](http://users.ece.utexas.edu/~adnan/gdb-refcard.pdf)</li></ul>
| <ul><li>[The C Preprocessor](https://www.math.utah.edu/docs/info/cpp_1.html)</li><li>[From Source to Binary: The Inner Workings of GCC](https://web.archive.org/web/20160410185222/https://www.redhat.com/magazine/002dec04/features/gcc/)</li><li>[The C Parser](https://github.com/gcc-mirror/gcc/blob/master/gcc/c/c-parser.c)</li><li>[ASCII Table](https://www.ascii-code.com)</li></ul>
|**Week 2**|
| <ul><li>[Think C](https://github.com/tscheffl/ThinkC/blob/master/PDF/Think-C.pdf) - chapter 9.</li><li>[More Datatypes](http://crasseux.com/books/ctutorial/More-data-types.html#More%20data%20types)</li><li>[Data Structures](http://crasseux.com/books/ctutorial/Data-structures.html#Data%20structures)</li></ul>
| <ul><li>[Think C](https://github.com/tscheffl/ThinkC/blob/master/PDF/Think-C.pdf) - chapters 6-8.</li><li>[Pointers](http://crasseux.com/books/ctutorial/Pointers.html#Pointers)</li><li>[Arrays](http://crasseux.com/books/ctutorial/Arrays.html#Arrays)</li></ul>
|**Week 1**|
| <ul><li>[C/C++ Extension for VSC](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools) (Good resource for getting your VSC environment configured)</li><li>[Think C](https://github.com/tscheffl/ThinkC/blob/master/PDF/Think-C.pdf) - chapters 1-5. This should be mostly review as it is analogous to Java</li><li>[Putting a Program Together](http://crasseux.com/books/ctutorial/Putting-a-program-together.html#Putting%20a%20program%20together) - this is new to the C language compared to Java</li></ul>
| [The Missing Semester](https://missing.csail.mit.edu) - Chapters 1, 2, 5, 6 |

## :vhs: Lectures - [Playlist](https://youtube.com/playlist?list=PL4A2v89SXU3TS1fcYFmXi-tch8p0LucE7) [Drive](https://drive.google.com/drive/folders/1SJgWVeEnhB9ql4i_MXrJL-H2D-OXvHbs?usp=sharing)

| Item                      | Date              | Content          | Links          |
| ------------------------- | ------------------ | ------------------ | --------------- |
|**Week 3**|
|Recitation 3 | 7/22 | Recitation 3 | [Video](https://drive.google.com/file/d/1ez23FJ0oRWGV7GlMdhJNAndh589vb_lb/view?usp=sharing)
|Lecture 10 | 7/21 | Compiling C Programs, Cont. | [Video](https://youtu.be/e9ghhbopWbg)
|Lecture 9 | 7/20 | Compiling C Programs | [Video](https://youtu.be/Hukb_0YFbhE)
|Lecture 8 | 7/19 | Memory Allocation in C | [Video](https://youtu.be/tNZw1RknRzQ)
|**Week 2**|
|Recitation 2| 7/15 | Recitation 2 | [Video](https://drive.google.com/file/d/1UH4sw8tfDaKbE5ufsDU94DdLxCWuYWNG/view?usp=sharing)
|Lecture 7 | 7/14 | Enums, Unions, Structs | [Video](https://youtu.be/8ebwJ7pWgyU)
|Lecture 6 | 7/13 | Pointers Cont. | [Video](https://youtu.be/bXt4uFQNhog)
|Lecture 5 | 7/12 | Pointers | [Video](https://youtu.be/AQ6WUlIT6i8)
|**Week 1**|
|Recitation 1| 7/9 | Recitation 1 | [Video](https://drive.google.com/drive/folders/1SJgWVeEnhB9ql4i_MXrJL-H2D-OXvHbs?usp=sharing)
|Lecture 4 | 7/7 | Anatomy of a C Program | [Video](https://youtu.be/Xh1LqbqKlo0) - [Code](https://gitlab.com/lehigh-cse-109/summer-2021/course-info/-/blob/master/project-template.zip)
|Lecture 3 | 7/7 | C and Unix | [Video](https://youtu.be/Bd21jOGT_GU)
|Lecture 2 | 7/6 | Introduction to Git and C | [Video](https://youtu.be/Y0pARcUxQmo)
|Lecture 1 | 7/6 | Course Introduction | [Video]()

